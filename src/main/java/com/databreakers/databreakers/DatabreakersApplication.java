package com.databreakers.databreakers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabreakersApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatabreakersApplication.class, args);
    }

}
