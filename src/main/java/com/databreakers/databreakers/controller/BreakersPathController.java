package com.databreakers.databreakers.controller;

import com.databreakers.databreakers.entity.BreakersPath;
import com.databreakers.databreakers.entity.ClosePoint;
import com.databreakers.databreakers.service.BreakersPathService;
import com.databreakers.databreakers.service.ClosePointService;
import io.jenetics.jpx.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.*;


@RestController
public class BreakersPathController {

    @Autowired
    private BreakersPathService breakersPathService;
    @Autowired
    private ClosePointService closePointService;

    @PostMapping(value="/processGPX",
            consumes = {MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public BreakersPath addBreakersPath(@RequestBody String gpxXML){
        final InputStream targetStream = new ByteArrayInputStream(gpxXML.getBytes());
        try {
            GPX gpx = GPX.read(targetStream);
            List<Track> tracks = gpx.getTracks();
            // Will only consider GPX with one track
            Boolean has_one_track = tracks.size() == 1;
            if (!has_one_track) {
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "GPX file can have only one track.");
            }
            // Process the GPX file and calculate detail info
            BreakersPath breakersPath = breakersPathService.processGPX(tracks.get(0));
            // Get the close points
            List<ClosePoint> closePoints = closePointService.createClosePoints(breakersPath);
            closePointService.saveAllClosePoints(closePoints);
            return breakersPathService.saveBreakersPath(breakersPath);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e);
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Cannot parse request body.", e);
        }
    }

    @GetMapping("/detail/{id}")
    public BreakersPath findBreakersPathById(@PathVariable int id){
        return breakersPathService.getBreakerPathById(id);
    }

    @GetMapping("/list")
    public List<BreakersPath> getBreakersPaths(){
        return breakersPathService.listBreakersPaths();
    }

}
