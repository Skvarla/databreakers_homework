package com.databreakers.databreakers.service;

import com.databreakers.databreakers.entity.BreakersPath;
import com.databreakers.databreakers.repository.BreakersPathRepository;
import com.databreakers.databreakers.structures.Pair;
import io.jenetics.jpx.Track;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import org.locationtech.jts.geom.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.apache.lucene.util.SloppyMath.haversinMeters;

@Service
public class BreakersPathService {

    @Autowired
    private BreakersPathRepository repository;

    public BreakersPath saveBreakersPath(BreakersPath path){
        return repository.save(path);
    }

    public List<BreakersPath> listBreakersPaths(){
        return repository.findAll();
    }

    public BreakersPath getBreakerPathById(int id){
        return repository.findById(id).orElse(null);
    }

    public BreakersPath processGPX(Track track){

        List<TrackSegment> segments = track.getSegments();
        Optional<String> track_name = track.getName();
        Boolean has_segments = segments.size() > 0;
        if (!has_segments){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Cannot parse request body.");
        }

        TrackSegment first_segment = segments.get(0);
        TrackSegment last_segment = segments.get(segments.size() - 1);
        Integer last_segment_point_size = last_segment.getPoints().size();
        Pair<Double, Double> start = new Pair<>(
                first_segment.getPoints().get(0).getLatitude().doubleValue(),
                first_segment.getPoints().get(0).getLongitude().doubleValue()
        );
        Pair<Double, Double> end = new Pair<>(
                last_segment.getPoints().get(last_segment_point_size-1).getLatitude().doubleValue(),
                last_segment.getPoints().get(last_segment_point_size-1).getLongitude().doubleValue()
        );
        Double air_distance = haversinMeters(start.getFirst(), start.getSecond(), end.getFirst(), end.getSecond());
        Optional<ZonedDateTime> startTime = first_segment.getPoints().get(0).getTime();
        Optional<ZonedDateTime> endTime = last_segment.getPoints().get(last_segment_point_size-1).getTime();
        Duration total_time = Duration.between(startTime.get(), endTime.get());

        // Calculate total distance (sum of distance of all segments)
        Double total_distance = 0.0;
        List<Coordinate> track_points = new ArrayList<>();
        for (TrackSegment s : segments) {
            total_distance += getSegmentDistance(s.getPoints());
            track_points.addAll(getSegmentPoints(s.getPoints()));
        }
        Double average_speed = 0.0;
        if (total_time.getSeconds() != 0) {
            average_speed = total_distance / total_time.getSeconds();
        }

        GeometryFactory g_factory = new GeometryFactory();
        BreakersPath breakersPath = new BreakersPath();
        Point startPoint = g_factory.createPoint(new Coordinate(start.getFirst(),start.getSecond()));
        Point endPoint = g_factory.createPoint(new Coordinate(end.getFirst(),end.getSecond()));
        LineString path = g_factory.createLineString(track_points.toArray(Coordinate[]::new));
        breakersPath.setAirDistance(air_distance);
        breakersPath.setAverageSpeed(average_speed);
        breakersPath.setName(track_name.get());
        breakersPath.setTotalDistance(total_distance);
        breakersPath.setPathStart(startPoint);
        breakersPath.setPathEnd(endPoint);
        breakersPath.setPath(path);
        breakersPath.setUploadedAt(LocalDateTime.now());
        return breakersPath;
    }

    private Double getSegmentDistance(List<WayPoint> points){
        Coordinate prev_coords = null;
        Double segment_distance = 0.0;
        for (WayPoint w : points) {
            Double lat = w.getLatitude().doubleValue();
            Double lng = w.getLongitude().doubleValue();
            if (prev_coords != null) {
                double dist = haversinMeters(
                        prev_coords.getX(),
                        prev_coords.getY(),
                        lat,
                        lng);
                segment_distance += dist;
                prev_coords.setX(lat);
                prev_coords.setY(lng);
            } else {
                prev_coords = new Coordinate(lat, lng);
            }
        }
        return segment_distance;
    }

    private List<Coordinate> getSegmentPoints(List<WayPoint> points){
        List<Coordinate> segment_points = new ArrayList<>();
        for (WayPoint w : points) {
            segment_points.add(new Coordinate(w.getLatitude().doubleValue(), w.getLongitude().doubleValue()));
        }
        return segment_points;
    }

}
