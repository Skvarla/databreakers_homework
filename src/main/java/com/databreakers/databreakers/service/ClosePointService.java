package com.databreakers.databreakers.service;

import com.databreakers.databreakers.entity.BreakersPath;
import com.databreakers.databreakers.entity.ClosePoint;
import com.databreakers.databreakers.repository.ClosePointRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;


@Service
public class ClosePointService {

    @Autowired
    private ClosePointRepository repository;
    @Autowired
    private Environment env;

    public List<ClosePoint> saveAllClosePoints(List<ClosePoint> points) {
        List<ClosePoint> response = repository.saveAll(points);
        return response;
    }

    public List<ClosePoint> createClosePoints(BreakersPath path){
        List<ClosePoint> closePoints = new ArrayList<>();
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();
        String places_url = String.format(
                "https://places.ls.hereapi.com/places/v1/discover/here?apiKey=%s&at=%f,%f&pretty",
                env.getProperty("apikey"),
                path.getPathEnd().getX(),
                path.getPathEnd().getY()
        );
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(places_url))
                .setHeader("User-Agent", "Java 11 HttpClient Bot")
                .build();
        HttpResponse<String> response = null;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            JSONObject jsonObject = new JSONObject(response.body());
            JSONArray items = (JSONArray) jsonObject.getJSONObject("results").get("items");
            for (int i = 0 ; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                String item_title = item.getString("title");
                Integer distance = item.getInt("distance");
                String categoty_title = item.getJSONObject("category").getString("title");
                ClosePoint closePoint = new ClosePoint(distance, item_title, categoty_title, path);
                closePoints.add(closePoint);
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return closePoints;
    }

}
