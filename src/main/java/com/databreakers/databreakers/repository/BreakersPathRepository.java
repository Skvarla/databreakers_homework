package com.databreakers.databreakers.repository;

import com.databreakers.databreakers.entity.BreakersPath;
import com.databreakers.databreakers.entity.ClosePoint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BreakersPathRepository extends JpaRepository<BreakersPath,Integer> {

}
