package com.databreakers.databreakers.repository;

import com.databreakers.databreakers.entity.ClosePoint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClosePointRepository extends JpaRepository<ClosePoint,Integer> {

}
