package com.databreakers.databreakers.entity;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="breakers_path")
public class BreakersPath {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    @Column(columnDefinition="geometry(Point)")
    private Point pathStart;
    @Column(columnDefinition="geometry(Point)")
    private Point pathEnd;
    private LineString path;
    private Double totalDistance;
    private Double airDistance;
    private Double averageSpeed;
    private LocalDateTime uploadedAt;
    @OneToMany(mappedBy="breakersPath")
    private Set<ClosePoint> closePoints;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getPathStart() {
        return pathStart;
    }

    public void setPathStart(Point pathStart) {
        this.pathStart = pathStart;
    }

    public Point getPathEnd() {
        return pathEnd;
    }

    public void setPathEnd(Point pathEnd) {
        this.pathEnd = pathEnd;
    }

    public LineString getPath() {
        return path;
    }

    public void setPath(LineString path) {
        this.path = path;
    }

    public Double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Double getAirDistance() {
        return airDistance;
    }

    public void setAirDistance(Double airDistance) {
        this.airDistance = airDistance;
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public LocalDateTime getUploadedAt() {
        return uploadedAt;
    }

    public void setUploadedAt(LocalDateTime uploadedAt) {
        this.uploadedAt = uploadedAt;
    }

    public Set<ClosePoint> getClosePoints() {
        return closePoints;
    }

    public void setClosePoints(Set<ClosePoint> closePoints) {
        this.closePoints = closePoints;
    }
}
