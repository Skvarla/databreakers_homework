package com.databreakers.databreakers.entity;

import javax.persistence.*;

@Entity
public class ClosePoint {

    @Id
    @GeneratedValue
    private int id;
    private Integer distance;
    private String title;
    private String categoryTitle;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="breakersPathId", nullable=false)
    private BreakersPath breakersPath;

    public ClosePoint() {
    }

    public ClosePoint(Integer distance, String title, String categoryTitle, BreakersPath path) {
        this.distance = distance;
        this.title = title;
        this.categoryTitle = categoryTitle;
        this.breakersPath = path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public BreakersPath getBreakersPath() {
        return breakersPath;
    }

    public void setBreakersPath(BreakersPath breakersPath) {
        this.breakersPath = breakersPath;
    }
}
